// set up basic variables for app

const record = document.querySelector('.record');
const stop = document.querySelector('.stop');
const soundClips = document.querySelector('.sound-clips');
const placeholder = document.getElementById('placeholder');
const apiKeyInput = document.getElementById('apikey');
const tooltip = document.querySelector('.tooltip');

// disable stop button while not recording
stop.disabled = true;

// record button disabled until openai api key is entered
function checkForApiKey() {
if (apiKeyInput.value.length > 0) {
  record.disabled = false;
  tooltip.classList.remove('no-api-key');
} else {
  record.disabled = true;
  tooltip.classList.add('no-api-key')
}
}


apiKeyInput.addEventListener('input', checkForApiKey);
checkForApiKey();

// main block for doing the audio recording

if (!navigator.mediaDevices.getUserMedia) {
  console.log('getUserMedia not supported on your browser!');
}

let mediaStream = null;
let mediaRecorder;
let chunks = [];
let transcriptionResponse;
const constraints = { audio: true };


record.onclick = async function() {
  try {
    mediaStream = await navigator.mediaDevices.getUserMedia(constraints);
    mediaRecorder = new MediaRecorder(mediaStream, { mimeType: 'audio/webm' });
    mediaRecorder.onstop = mediaRecorderStopHandler;
    mediaRecorder.ondataavailable = function(e) {
      chunks.push(e.data);
    }
    mediaRecorder.start();
    record.style.background = "coral";
    record.style.color = "white";
    stop.disabled = false;
    record.disabled = true;
  } catch (err) {
    console.log('The following error occured: ' + err);
  }
}

stop.onclick = function() {
  mediaRecorder.stop();
  record.style.background = "";
  record.style.color = "";
  stop.disabled = true;
  record.disabled = false;
  if (mediaStream) {
    mediaStream.getTracks().forEach(track => track.stop());
    mediaStream = null;
    console.log('Microphone access stopped');
  }
}

function mediaRecorderStopHandler(e) {
  placeholder.textContent = "";
  const clipContainer = document.createElement('article');
  const clipLabel = document.createElement('p');
  const audioContainer = document.createElement('div');
  const textContainer = document.createElement('div');
  const audio = document.createElement('audio');
  const deleteButton = document.createElement('button');
  const transcribeButton = document.createElement('button');
  const copyButton = document.createElement('button');
  const timestamp = document.createElement('p');

  clipContainer.classList.add('clip');
  audioContainer.classList.add('audio-container');
  textContainer.classList.add('text-container');
  audio.classList.add('audio');
  copyButton.classList.add('copy-button');
  clipLabel.textContent = 'Press transcribe to get text.';
  deleteButton.textContent = 'Delete';
  transcribeButton.textContent = 'Transcribe';
  copyButton.textContent = 'Copy';
  timestamp.textContent = new Date().toLocaleString();
  audio.setAttribute('controls', '');
  
  clipContainer.appendChild(audioContainer);
  audioContainer.appendChild(transcribeButton);
  audioContainer.appendChild(audio);
  audioContainer.appendChild(deleteButton);
  textContainer.appendChild(copyButton);
  textContainer.appendChild(clipLabel);
  clipContainer.appendChild(textContainer);
  clipContainer.appendChild(timestamp);
  soundClips.prepend(clipContainer);

  audio.controls = true;
  const blob = new Blob(chunks, { 'type' : 'audio/webm' });
  chunks = [];
  const audioURL = window.URL.createObjectURL(blob);
  audio.src = audioURL;

  // Transcribe button handler
  transcribeButton.onclick = async function(e) {
    transcribeButton.textContent = 'Transcribing...';
    const formData = new FormData();
    const file = new File([blob], "input.webm", { type: "audio/webm" });
    formData.append('file', file);
    formData.append('model', 'whisper-1');
    const headers = {
      'Authorization': `Bearer ${apiKeyInput.value}`,
    };
    const r = await fetch('https://api.openai.com/v1/audio/transcriptions', {
      method: 'POST',
      headers: headers,
      body: formData
    });
    if (r.status === 200) {
      const data = await r.json();
      transcriptionResponse = data.text;
    } else {
      transcriptionResponse = `Error: ${r.status} | ${r.statusText} | See console for details.`;
      console.error(r);
    }
    clipLabel.textContent = transcriptionResponse;
    transcribeButton.textContent = 'Transcribe';
    return true;
  }

  // Delete button handler
  deleteButton.onclick = function(e) {
    e.target.closest(".clip").remove();
  }

  // Copy button handler
  copyButton.onclick = function(e) {
    navigator.clipboard.writeText(clipLabel.textContent);
    copyButton.classList.add('animate');

    setTimeout(() => {
      copyButton.classList.remove('animate');
    }, 1000);
  }
}
